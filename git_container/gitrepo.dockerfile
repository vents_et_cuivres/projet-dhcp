FROM debian:latest
LABEL description="Container hébergeant le dépôt Git de la config DHCP"
LABEL version="2.0"

# Updating container
RUN apt update && apt upgrade -y
RUN apt install -y software-properties-common \
		git \
		openssh-server
RUN apt clean && apt autoremove

# Creating `git` user
RUN useradd -r -m -U -d /home/git -s /bin/bash git
RUN echo 'git:secret' | chpasswd

# Setting up SSH Server
RUN mkdir -p /run/sshd
RUN sshd -t 
EXPOSE 22/tcp
CMD /usr/sbin/sshd -De

# Preparing `git` user files
RUN mkdir /home/git/.ssh && chown -R git:git /home/git/.ssh && chmod 0700 /home/git/.ssh
COPY ./ssh_files/git_authorized_keys /home/git/.ssh/authorized_keys
RUN chown -R git:git /home/git/.ssh/authorized_keys && chmod 0600 /home/git/.ssh/authorized_keys

# Creating Git repo
RUN git init --bare /home/git/dhcp-config.git
RUN chown -R git:git /home/git/dhcp-config.git
